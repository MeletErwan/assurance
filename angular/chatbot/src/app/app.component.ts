import { Component, OnInit, Input, Inject } from '@angular/core';
import {ChatDialogComponent} from './chat/chat-dialog/chat-dialog.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'chatbot';
  connect: boolean;

  constructor(public chatModule: ChatDialogComponent){
    this.connect = false;
  }

  switchConnect(){
    this.connect = !this.connect;
    this.chatModule.status = this.connect;
    this.chatModule.reset();

  }
}
