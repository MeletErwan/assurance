import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment'
import {HttpClient, HttpResponse}  from "@angular/common/http";
import { Observable } from 'rxjs';
import { BehaviorSubject  } from 'rxjs'
import * as fileSaver from 'file-saver';

@Injectable({
  providedIn: 'root'
})

export class Message{
  constructor(public content: string, public sendBy: string){}
}

@Injectable()
export class ChatService {

  readonly token = environment.dialogFlow.angularBot;
  conversation = new BehaviorSubject<Message[]>([])
  status: boolean; 

  constructor(public http: HttpClient ) {

    let firstMsg = new Message(`Bonjour , je m’appelle SamBot !
    Si vous avez déjà un compte, merci de vous connecter 
    pour une expérience plus personnalisée :)  
    Comment puis-je vous aider ?`, "bot")
    this.update(firstMsg)
  }

  update(msg: Message){
    this.conversation.next([msg]);
  }

  converse(msg: string){
    const userMessage = new Message(msg, 'user');
    this.update(userMessage);
    const headers = { 'Authorization': 'Bearer '+this.token };
    const body = {
      "queryInput":
      {"text":
          {"text":msg,"languageCode":"fr"}
  
      }
  };
    this.http.post<any>('https://dialogflow.clients6.google.com/v2/projects/newagent-dafd/agent/sessions/1916f7fe-a821-c47c-19ad-1b4ebe6c4c48:detectIntent', body, { headers }).subscribe(data => {
    const botMessage = new Message(data.queryResult.fulfillmentText, 'bot');
      this.update(botMessage);
      if(data.queryResult.diagnosticInfo){
        if(data.queryResult.diagnosticInfo.end_conversation){
          this.download()
        }
      }
    });
  }

  updateStatus(s){
    this.status = s;
  }

  sendData(data){
    
  }
  download() {
    //this.fileService.downloadFile().subscribe(response => {
     this.downloadFile().subscribe((response: any) => { //when you use stricter type checking
       let blob:any = new Blob([response], { type: 'text/json; charset=utf-8' });
       const url = window.URL.createObjectURL(blob);
       //window.open(url);
       //window.location.href = response.url;
       fileSaver.saveAs(blob, 'contrat-auto.pdf');
     //}), error => console.log('Error downloading the file'),
     }), (error: any) => console.log('Error downloading the file'), //when you use stricter type checking
                  () => console.info('File downloaded successfully');
   }
  resetForConnect(){
    this.update(new Message(`Bonjour Anaïs, je m’appelle SamBot ! Comment puis-je vous aider ?
    Si vous avez subi un sinistre dites : sinistre
    Si vous avez des questions relatives à votre contrat dites : mon contrat
    Si vous avez d’autres questions dites : question ?`, "bot"))
    //this.downloadFile()
  }

  downloadFile(): any {
    const headers1 = { 'Access-Control-Allow-Origin':'*'};
	 return this.http.get('http://localhost/test/votre-assurance-auto.pdf', {headers:headers1, responseType: 'blob'});
  }
}
