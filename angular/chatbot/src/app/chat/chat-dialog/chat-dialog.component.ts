import { Component, OnInit, Input, Injectable, OnChanges, SimpleChanges, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { ChatService } from '../chat.service';
import {Observable } from 'rxjs'
import { scan } from 'rxjs/operators';
import * as fileSaver from 'file-saver';
import { Message } from '../chat.service'

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.css']
})

export class ChatDialogComponent implements OnInit, OnChanges, AfterViewChecked {
  public messages: Observable<Message[]>;
  public connect: Observable<boolean>;
  formValue: string;
  public tests: Message[]; 
  public acc: Message[];

  @Input()
  status: boolean;
  @ViewChild('scrollme') private myscrollContainer: ElementRef


  constructor(private chat: ChatService) {
      this.acc = []
   }

  ngOnInit() {
    this.chat.conversation.asObservable().pipe()
      .subscribe(test => {

        this.acc = this.acc.concat(test);
        this.acc;
        this.tests = this.acc;
      });
      
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
    this.myscrollContainer.nativeElement.scrollTop = this.myscrollContainer.nativeElement.scrollHeight;
    } catch(err) {
      
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.status.currentValue) {
      this.reset()
      this.chat.resetForConnect()
      
    }


  }

  sendMessage(){
    
    this.chat.converse(this.formValue);
    this.formValue = '';
  }

  reset(){
    this.tests = [];
    this.acc = []
  }
}


