# Projet SamBot

## Présentation du dossier

1. SamBot.zip --> contient le bot sous format ZIP
2. angular/chatbot --> contient le front avec l'interface utilisateur
3. Presentation --> Contient la présentation


## Choix des technologies

1. Pour le backend du chatbot le service DialogFlow a été utilisé https://dialogflow.cloud.google.com/
    Il permet de creer un chatbot et de renseigner les questions et les reponses attendues par l'utilisateur. Ainsi que de préformater les réponses : format date, string ou en donnant un modèle personnalisé
2. Pour la partie Front la technologie Angular a été choisie


## Utilisation de l'application

1. Pour DialogFlow
Se rendre sur l'adresse : https://dialogflow.cloud.google.com/
Créer un nouvel agent et importer les "Intends" du zip

<img src="./img/import.png">

Une fois le bot importé il faudra se rendre sur le Google could pour gerer les autorisations https://console.cloud.google.com/home/dashboard
Pour notre projet nous avons généré des Tokens avec oAuth2. voici un exemple de documentation pour configurer l'authentification : https://developers.google.com/identity/protocols/oauth2/service-account


2. Pour Angular

Npm est necessaire pour lancer le projet

Lancer les commandes suivantes dans le repertoire chatbot:
```
npm install -g @angular/cli
npm install
ng serve
```
Generer le token avec oAuth2 permettant de donner l'autorisation de dialoguer avec le Bot et le renseigner dans le fichier "environments/environement.ts" à la racine du projet

Une fois le serveur en route se rendre sur l'adresse IP suivante http://localhost:4200

Enjoy :)